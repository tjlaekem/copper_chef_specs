\documentclass{article}

\usepackage[margin=1in]{geometry}
\usepackage{graphicx}
\usepackage{gensymb}
\usepackage{tabularx}
\usepackage{setspace}
\usepackage{float}
\floatstyle{plaintop}
\restylefloat{table}
\usepackage[tableposition=top]{caption}
\usepackage{enumitem}

\begin{document}

\pagenumbering{gobble}

\begin{titlepage}
	\centering

	{\huge University of Waterloo \\}
	{\Large Faculty of Engineering \\}
	{\Large Department of Electrical and Computer Engineering}

	\vspace{1.2in}

	{\Huge The Copper Chef}

	\vspace{1.2in}

	{\Large Group 2018.050 \\}

	\vspace{1.2in}

	{\Large Project Consultant \\}
	{\large Professor Christopher Nielsen}

	\vspace{1.2in}

	{\Large Prepared By \\}

	\begin{table}[h!]
	\centering
	\begin{tabular}{l l r}
		{\large Eric Bender} & {\large etbender@uwaterloo.ca} & {\large 20534384} \\
		{\large Shiva Ehtezazi} & {\large sehtezaz@uwaterloo.ca} & {\large 20529687} \\
		{\large Taylor Laekeman} & {\large tjlaekem@uwaterloo.ca} & {\large 20484137} \\
		{\large Christian Petri} & {\large capetri@uwaterloo.ca} & {\large 20484960} \\
		{\large Brian Truong} & {\large b3truong@uwaterloo.ca} & {\large 20528564}
	\end{tabular}
	\end{table}
\end{titlepage}

\tableofcontents
\newpage

\pagenumbering{arabic}

\section{High-Level Description}

This section will provide a high-level description of the project that includes descriptions of its motivation and objectives, and a block diagram.

\subsection{Motivation}

Many variables affect the quality of a cooked steak, which should finish within a very narrow range of internal temperature, and with a good, even sear on each side \cite{doneness}.  If the cooking surface is too hot, the sides will burn instead of sear, and it becomes very easy to overshoot the desired internal temperature.  If the cooking temperature is too low, the sear will be imperfect, and the steak will lack the flavor contributed by the Maillard reaction \cite{maillard}.  If the steak is flipped too early or too late, the sides will not be seared evenly, and the cooking gradient through the steak will not be consistent.  For the amateur chef, or even just someone who doesn’t want to devote them self to the culinary arts, the perfect steak is rare and elusive.  If the chef is trying to simultaneously prepare a range of side dishes, the problem is magnified.

If the manually cooked steak is lacking, the chef has few options to satisfy their carnivorous craving.  They could try to convince a friend to cook for them, but it’s not always possible to find somebody capable of improving on the first failed attempt.  They could go to a restaurant, but a steak from a restaurant will be much more expensive than a steak prepared at home.  Unless they are willing to devote themselves---academically or financially---to the perfect steak, the amateur will lead a sad, hungry life, lacking in succulence and flavor, and occupied instead with dry, overcooked, and generally substandard meat.


\subsection{Project Objective}

The objective of this project is to design a build a device capable of automatically cooking the perfect steak. The user will load the device with a steak and indicate their preferred doneness. By monitoring the characteristics of the steak, the device will determine when the steak is done and notify the user that the steak is ready.  Upon completion, the steak will be isolated from the heat source, removing the need for immediate action.  The device will provide the user with the simplicity of ordering a steak in a restaurant at the cost of a home-cooked steak.  The autonomy provides the user time to step away from the kitchen, or to focus their attention on side dishes they may be preparing.  The device ensures the steak is cooked to the desired doneness every time without the hassle of monitoring it yourself, eliminating the potential a ruined main entree.  

\newpage
\subsection{Block Diagram}

The block diagram displayed in Figure 1 shows the high level design that will be implemented in the construction of The Copper Chef.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.8\linewidth]{res/block_diagram.png}
	\caption{Block diagram of The Copper Chef}
\end{figure}

The device will consist of three major subsystems:

\begin{itemize}
	\item a mechanical subsystem that will manage the contact between the steak and the cooking element
	\item a sensory subsystem that will monitor the relevant physical characteristics of the steak, specifically its temperature
	\item an input/output subsystem, that will receive configuration from and communicate status to the user
\end{itemize}

All three subsystems will be controlled by the microcontroller. The device will receive power from a typical household circuit, and this power will be distributed to the subsystems; the distribution system will give each subsystem the voltage and power it requires to function as designed.

\newpage
\section{Project Specifications}

This section will present the functional and non-functional specifications of the project.

\subsection{Functional Specifications}

Table 1 shows the functional specifications of The Copper Chef.

\begin{table}[h!]
\begin{tabularx}{\textwidth}{l l X}
	\hline \\

	Specification & Necessity & Description \\ \\

	\hline \\

	Internal Temperature & Essential & The internal temperature of the cooked steak must be within 1.5 \degree C of the target temperature, as determined by the desired doneness of the steak. \\ \\

	Autonomy & Essential & The machine functions without intervention from the user;  after the steak is loaded, the machine will function independently until cooking has finished. \\ \\

	Capacity & Essential & The device must be able to cook bone-out steaks up to 1 lb (454 g) in mass. \\ \\

	Patience & Non-Essential & The device must be able to hold the steak away from the heat source until it has determined that the cooking surface has reached the correct temperature. \\ \\

	Doneness Selection & Non-Essential & The user must be allowed to select from a range of doneness options as specified by Certified Angus Beef \cite{doneness}:
	\begin{itemize}
		\item Rare (52 \degree C)
		\item Medium-Rare (57 \degree C)
		\item Medium (63 \degree C)
		\item Medium-Well (66 \degree C)
		\item Well-Done (71 \degree C)
	\end{itemize} \\ 

	Notification & Non-Essential & The user must be notified when the steak has finished cooking. \\ \\

	\hline
\end{tabularx}
\caption{Functional specifications of The Copper Chef}
\end{table}

\newpage
\subsection{Non-functional Specifications}

Table 2 shows the non-functional specifications of The Copper Chef.

\begin{table}[h!]
\begin{tabularx}{\textwidth}{l l X}
	\hline \\

	Specification & Necessity & Description \\ \\

	\hline \\

	Heat Resistance & Essential & The device must be able to withstand the cooking heat to which it is exposed; components must not melt, smoke, or otherwise degrade due to the temperatures at which the device is regularly used.  \\ \\

	Volume \& Shape & Essential & The operating dimensions of the device must be within 380 mm (width) x 330 mm (length) x 610 mm (height), where the height is measured from the flat surface on which the system sits.  These dimensions need not include the system that will interface with the external power source. \\ \\

	Power & Essential & The device must be compatible with a typical household 15A, 120V receptacle. \\ \\

	Sanitation & Essential & All components that come into contact with the food must be removable in order to facilitate cleaning in a standard kitchen sink. \\ \\

	Cooking Time & Non-Essential & The device must cook a steak in at most 45 minutes, the cooking time of the Cinder, a competing automated steak-cooking system. \\ \\

	Cost & Non-Essential & The device must cost less than \$ 429 USD, the cost of the Cinder \cite{cinderprice}. \\ \\

	Weight & Non-Essential & The device must weigh less than 15 lbs (6.8 kg), the weight of a Lodge Enameled Cast Iron Dutch Oven, a heavy but frequently-moved kitchen tool \cite{lodgeweight}. \\ \\

	\hline
\end{tabularx}
\caption{Non-functional specifications of The Copper Chef}
\end{table}

\newpage
\section{Risk Assessment}

This section will assess the risks that could prevent the successful completion of the project.  The risks, their likelihoods and impacts, and mitigation efforts are presented in Table 3.

\begin{table}[h!]
\begin{tabularx}{\textwidth}{p{0.6\textwidth} X X}
	\hline \\

	Risk & Probability & Impact \\ \\

	\hline \\

	Insufficient mechanical engineering knowledge & Medium & High \\ 
	\multicolumn{3}{p{\textwidth}}{
		\begin{itemize}[itemsep=0pt, topsep=0pt]
			\item The project requires significant mechanical design, in which the group is not formally trained or greatly experienced.
			\item The risk of an insurmountable mechanical engineering problem has been mitigated through the selection of a consultant with expertise in mechatronics and mechanical engineering.
		\end{itemize}} \\

	Insufficient electrical engineering knowledge & Low & High \\ 
	\multicolumn{3}{p{\textwidth}}{
		\begin{itemize}[itemsep=0pt, topsep=0pt]
			\item While there are three electrical engineering students in the group, the possibility remains that the project will present problems that are beyond their combined capabilities.
			\item Should an insurmountable problem arise, the group is well situated to approach experts in various electrical engineering domains, including professors at the University of Waterloo and family members with professional engineering designations.
		\end{itemize}} \\

	Insufficient software engineering knowledge & Low & High \\ 
	\multicolumn{3}{p{\textwidth}}{
		\begin{itemize}[itemsep=0pt, topsep=0pt]
			\item While there are two software-focused computer engineering students in the group, the possibility remains that the project will present problems that are beyond their combined capabilities.
			\item Should an insurmountable problem arise, the group is well situated to approach experts in various software engineering domains, notably professors at the University of Waterloo.
		\end{itemize}} \\

	Lack of group cohesion & High & Medium \\ 
	\multicolumn{3}{p{\textwidth}}{
		\begin{itemize}[itemsep=0pt, topsep=0pt]
			\item Coordinating and managing the ideas, opinions, feelings, and work habits of five people is a difficult task.  It is expected that the completion of the project will feature miscommunications and interpersonal conflicts.
			\item The group will have a minimum of three scheduled meetings each week in order to avoid stressful crunches around deadlines.  Slack is being used to engender strong communication protocols.  Responsibilities will be clearly defined and explicitly divided among group members, to ensure an equality of effort and avoid last-minute, group-wide scrambles.  Social, non-working meetings and events will be scheduled to maintain the interpersonal relationships of group members.
		\end{itemize}} \\

	Inability to acquire adequate components & Low & High \\ 
	\multicolumn{3}{p{\textwidth}}{
		\begin{itemize}[itemsep=0pt, topsep=0pt]
			\item Components in the device will need to satisfy numerous criteria, including precision, and high-temperature operation.  It is possible that such components cannot be found, that such components components cannot be found at an achievable price, or that such components can be ordered individually, or in reasonably small number.
			\item The group has produced multiple candidate designs that are capable of satisfying the project specifications.  If specific parts cannot be found, efforts can shift to a less-optimal design that features components that are attainable.
		\end{itemize}} \\

	\hline
\end{tabularx}
\caption{Risk assessment}
\end{table}

\newpage
\begin{thebibliography}{4}

\bibitem{doneness}
"Degree of Doneness", Certifiedangusbeef.com, 2017. [Online]. Available: https://www.certifiedangusbeef.com/kitchen/doneness.php. [Accessed: 29- May- 2017].

\bibitem{maillard}
"Why does Food Brown when Cooked", Scienceofcooking.com, 2017. [Online]. Available: http://www.scienceofcooking.com/maillard\_reaction.htm. [Accessed: 29- May- 2017].

\bibitem{cinderprice}
"Cinder Grill: Cook Food Perfectly", Indiegogo, 2017. [Online]. Available: https://www.indiegogo.com/projects/cinder-grill-cook-food-perfectly-cooking\#/. [Accessed: 29- May- 2017].

\bibitem{lodgeweight}
"Amazon.com: Lodge EC6D33 Enameled Cast Iron Dutch Oven, 6-Quart, Blue: Kitchen \& Dining", Amazon.com, 2017. [Online]. Available: https://www.amazon.com/Lodge-EC6D33-Enameled-Dutch-6-Quart/dp/B000N4WN08. [Accessed: 29- May- 2017].

\end{thebibliography}

\end{document}
